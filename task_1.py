class Car:
    def __init__(self) -> None:
        self._num_wheels = 4

    def get_num(self):
        return self._num_wheels
    
    def set_num(self, num):
        self._num_wheels = num
    
    num_wheels = property(get_num, set_num)

car = Car()

print(car.num_wheels)
car.num_wheels = 6
print(car.num_wheels)