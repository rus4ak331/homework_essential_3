class English:
    def __init__(self, name) -> None:
        self.name = name

    def greeting(self):
        return f'Hello {self.name}'
    

class Spanish:
    def __init__(self, name) -> None:
        self.name = name

    def greeting(self):
        return f'Hola {self.name}'
    

def hello_friend(peoples):
    for people in peoples:
        print(people.greeting())

peoples = [
    English('Bob'),
    Spanish('Max')
]

hello_friend(peoples)