class Temperature:
    def __init__(self, celsius=None, fehrenheit=None) -> None:
        self.celsius = celsius
        self.fahrenheit = fehrenheit

    @property
    def celsius(self):
        if self._fahrenheit != None:
            self._celsius = (self._fahrenheit - 32) / 1.8
        return self._celsius
    
    @celsius.setter
    def celsius(self, value):
        self._celsius = value
        self._fahrenheit = None

    @property
    def fahrenheit(self):
        if self._celsius != None:
            self._fahrenheit = (self._celsius * 1.8) + 32
        return self._fahrenheit

    @fahrenheit.setter
    def fahrenheit(self, value):
        self._fahrenheit = value


temp = Temperature(celsius=20)
print(temp.celsius)
print(temp.fahrenheit)
print()

temp.celsius = 15
print(temp.celsius)
print(temp.fahrenheit)
print()

temp.fahrenheit = 68
print(temp.celsius)
print(temp.fahrenheit)
print()

temp2 = Temperature(fehrenheit=68)
print(temp2.fahrenheit)
print(temp2.celsius)