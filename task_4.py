class Base:
    @classmethod
    def method(cls):
        print(f'hello from {cls.__name__}')


class Child(Base):
    def __init__(self) -> None:
        super().__init__()


Base.method()
Child.method()